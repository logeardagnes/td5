package td5;

class Avion extends Thread {
	
	private String nom;
	private Aeroport a;
	
	public Avion(String s){
		this.nom=s;
	} 
	
	public void run(){ 
		this.a=Aeroport.getInstance();
		System.out.println("Je suis avion "+this.nom+" sur aeroport "+this.a);
		while(this.a.autoriserAdecoller() == false){
			
			System.out.println(this.nom+" attend");
			try {
				Thread.sleep((int) Math.random()* 200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		System.out.println(this.nom+" de�colle");
		try {
			this.sleep((long)Math.random());
		}catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		this.a.liberer_piste();
		System.out.println(this.nom+" vole");
	} 
}