package td5;

import java.awt.*;
import java.util.*;
import javax.swing.*;


public class Simulateur extends JPanel {
	
	private FabriqueIntersection fi;
	
	public Simulateur(FabriqueIntersection fabriI){
		fi = fabriI;
	}
	
	public Hashtable<String, Integer> genererStats(){
		Vehicule v;
		Hashtable<String, Integer> h = new Hashtable<String, Integer>();
		int nbVoiture=0;
		int nbBus=0;
		int nbPieton=0;
		int nbBic=0;
		for(int i=1; i<=100;i++){
			v = fi.creerVehicule();
			if(v instanceof Voiture){
				nbVoiture++;
			}else{
				if(v instanceof Bus){
					nbBus++;
				}else{
					if(v instanceof Pieton){
						nbPieton++;
					}else{
						nbBic++;
					}
				}
			}
		}
		h.put("Voiture", nbVoiture);
		h.put("Bus", nbBus);
		h.put("Pieton", nbPieton);
		h.put("Bicyclette", nbBic);
		return h;
	}
	
	public void EcrireStats(){
		String type;
		Hashtable<String, Integer> h = new Hashtable<String, Integer>();
		h = this.genererStats();
		Set cles= h.keySet();
		Iterator it = cles.iterator();
		System.out.println("Pour 100 vehicule fabriquer il y a eu :");
		while(it.hasNext()){
			type = (String) it.next();
			System.out.println(h.get(type)+" "+type);
		}
	}
	
	public void DessinerStats(){
		JFrame fenetre = new JFrame();
	    fenetre.setTitle("Statistique Vehicule");
	    fenetre.setSize(500, 500);	//D�finit sa taille
	    fenetre.setLocationRelativeTo(null); //Centrer la fenetre
	    fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Termine le processus lorsqu'on clique sur la croix rouge       
	    fenetre.setVisible(true);
	    MesRectangles mesRec = new MesRectangles();
	    mesRec.ajouterFabrique(this);
	    fenetre.setContentPane(mesRec);
	}
	
	
	
		public static class MesRectangles extends JPanel{
			private Simulateur simu;
			
			public void ajouterFabrique(Simulateur Sim){
				simu = Sim;
			}
			public void paintComponent(Graphics g){
				//Rectangle principale
				g.fillRect(10, 10, 400, 100); //(ecart x, ecarty,taille hauteur, taille longueur )
				g.fillRect(190,140 ,90 ,80 );
				String s;
				int taille;
				int deplacer;
				int tailleAffiche;
				Hashtable<String, Integer> h = new Hashtable<String, Integer>();
				h = (this.simu).genererStats();
				s = "Voiture";
				taille=h.get(s);
				deplacer=taille+10;
				g.setColor(Color.WHITE);
				g.fillRect(10, 10, 400, taille);
				g.drawString("Voiture",200,150);
				s = "Bus";
				taille=h.get(s);
				g.setColor(Color.RED);
				g.fillRect(10, deplacer, 400, taille);
				g.drawString("Bus",200 ,170);
				deplacer=deplacer+taille;
				s = "Pieton";
				taille=h.get(s);
				g.setColor(Color.BLUE);
				g.fillRect(10, deplacer, 400, taille);
				g.drawString("Pieton",200 ,190);
				deplacer=deplacer+taille;
				s = "Bicyclette";
				taille=h.get(s);
				g.setColor(Color.GREEN);
				g.fillRect(10, deplacer, 400, taille);
				g.drawString("Bicyclette",200 ,210);
			}               
		}

}
