package td5;


public abstract class Vehicule {
	private double vitesse;
	private double vitesseMax;
	private String type;
	
	public Vehicule(double vitM, String typ){
		vitesseMax = vitM;
		type = typ;
		vitesse=0;
	}
	
	public double getVitesse(){
		return vitesse;
	}
	
	public String getType(){
		return type;
	}
	
	public void accelerer(double v){
		if((vitesse + v) <= vitesseMax){
			vitesse = vitesse + v;
		}
	}
	
	public void decelerer(double v){
		if((vitesse - v) >= 0){
			vitesse = vitesse - v;
		}
	}
	
}
