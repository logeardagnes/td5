package td5;


public class MainSimulation {
	public static void main(String[] args){
		FabriqueIntersection fi = new FabriqueIntersection();
		FabriqueIntersection fi2 = new FabriqueIntersection(0.3,0.5,0.1,0.1);
		Simulateur s = new Simulateur(fi);
		Simulateur s2 = new Simulateur(fi2);
		s.EcrireStats();
		System.out.println("\n");
		s2.EcrireStats();
		s.DessinerStats();
	}

}
