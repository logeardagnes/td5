package td5;

public class Aeroport{
	private boolean piste_libre;
	private static Aeroport SingletonAero;
	
	private Aeroport(){
		this.piste_libre=true; 
	}
	
	public static synchronized Aeroport getInstance(){
		if(SingletonAero == null){
			SingletonAero = new Aeroport();	
		}
		return SingletonAero;
	}
	
	public synchronized boolean autoriserAdecoller(){
		if(piste_libre  == true){
			piste_libre = false;
			return true;
		}
		else {
			return false;
		}
	}	
	
	public synchronized boolean liberer_piste(){
		piste_libre = true;
		return true;
	
	}
}
