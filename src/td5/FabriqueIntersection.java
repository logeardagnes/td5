package td5;



public class FabriqueIntersection{
	private double probaVoiture;
	private double probaBus;
	private double probabyc;
	private double probaPieton;
	
	public FabriqueIntersection(){
		probaVoiture = 0.8;
		probaPieton = 0.1;
		probaBus = 0.05;
		probabyc = 0.05;
	}
	
	public FabriqueIntersection(double v, double p, double bu, double by){
		if(((v+p+bu+by)== 1) && v >= 0 && p >= 0 && bu >= 0 && by >= 0 ){
			probaVoiture = v;
			probaPieton = p;
			probaBus = bu;
			probabyc = by;
		}
		else{
			System.out.println("Probleme dans les variables");
		}
	}
	
	public Vehicule creerVehicule(){
		double proba = (double) Math.random();
		Vehicule v = null;
		if(proba <= probaVoiture && proba >0 ){
			return (v=new Voiture());
		}else{
			if(proba <= probaVoiture+ probaPieton && proba >probaVoiture){
				return (v=new Pieton());
			}
			else{
				if(proba <= probaVoiture+ probaPieton + probaBus && proba >probaVoiture+ probaPieton){
					return (v=new Bus());
				}
				else{
					return (v=new Bicyclette());
				}
			}
		}
	}
}
